// Link: https://www.hackerearth.com/practice/basic-programming/implementation/basics-of-implementation/practice-problems/algorithm/array-sum-2-725368ac/

#define ll long long
 
#include <bits/stdc++.h>
using namespace std;
int main() 
{
	ll n, ans = 0;
	cin >> n;
 
	for(int i = 0; i < n; i++)
	{
		ll digit;
		cin >> digit;
		ans += digit;
	}
 
	cout << ans << endl;
}

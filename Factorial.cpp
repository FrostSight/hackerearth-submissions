// Link: https://www.hackerearth.com/practice/basic-programming/input-output/basics-of-input-output/practice-problems/algorithm/find-factorial/

#include <iostream>
using namespace std;
 
int factorial(int num)
{
	int ans;
 
	if(num == 0 || num == 1)
		return 1;
	
	ans = num * factorial(num-1);
	return ans;
}
 
int main() 
{
	int num, ans;
	cin >> num;  
 
	ans = factorial(num);
	cout << ans << endl;
}

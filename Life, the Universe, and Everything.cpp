// LinK: https://www.hackerearth.com/practice/basic-programming/input-output/basics-of-input-output/practice-problems/algorithm/life-the-universe-and-everything/

#include <iostream>
using namespace std;
int main() 
{
	int num;
 
	while(1)
	{
		cin >> num;
		if(num == 42)
			break;
		else
			cout << num << endl;
	}
}
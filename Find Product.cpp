// Link: https://www.hackerearth.com/practice/basic-programming/input-output/basics-of-input-output/practice-problems/algorithm/find-product/

#include <iostream>
using namespace std;
 
int main() 
{
    int num;
    cin >> num;
 
    long long res = 1;
    const int MOD = 1000000007;
 
    for (int i = 0; i < num; i++) {
        int input;
        cin >> input;
        res = (res * input) % MOD;
    }
 
    cout << res << endl;
}
